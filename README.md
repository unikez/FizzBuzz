# FizzBuzz


- [x] Formulaire avec validation
- [x] Navigation entre les pages
- [x] Affichage d'une liste
- [x] Injection de dépendance (koin)
- [x] Multi-modules (domain, data, presentation, app)
- [x] Tests unitaires (domain)