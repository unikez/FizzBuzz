package com.frgconsulting.fizzbuzz

import android.app.Application
import com.frgconsulting.fizzbuzz.di.AppModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class FizzBuzzApp : Application() {

    override fun onCreate() {
        super.onCreate()
        // init koin modules
        startKoin {
            androidLogger()
            androidContext(this@FizzBuzzApp)
            modules(AppModule().modules)
        }
    }
}