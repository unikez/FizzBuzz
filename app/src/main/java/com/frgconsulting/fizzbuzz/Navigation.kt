package com.frgconsulting.fizzbuzz

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import androidx.paging.compose.collectAsLazyPagingItems
import com.frgconsulting.fizzbuzz.presentation.Constants.LIMIT_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Constants.NUMBER1_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Constants.NUMBER2_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Constants.WORD1_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Constants.WORD2_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Screens
import com.frgconsulting.fizzbuzz.presentation.home.HomeScreen
import com.frgconsulting.fizzbuzz.presentation.home.HomeViewModel
import com.frgconsulting.fizzbuzz.presentation.result.ResultScreen
import com.frgconsulting.fizzbuzz.presentation.result.ResultViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun Navigation(navController: NavHostController) {
    NavHost(navController = navController, startDestination = Screens.Home.route) {
        composable(Screens.Home.route) {
            val homeViewModel = getViewModel<HomeViewModel>()
            val state = homeViewModel.state
            HomeScreen(
                state = state,
                onEvent = homeViewModel::onEvent,
                navigateTo = { route ->
                    navController.navigate(route)
                }
            )
        }

        composable(Screens.Result.route, arguments = listOf(
            navArgument(NUMBER1_ARGUMENT_KEY) { type = NavType.StringType },
            navArgument(NUMBER2_ARGUMENT_KEY) { type = NavType.StringType },
            navArgument(WORD1_ARGUMENT_KEY) { type = NavType.StringType },
            navArgument(WORD2_ARGUMENT_KEY) { type = NavType.StringType },
            navArgument(LIMIT_ARGUMENT_KEY) { type = NavType.StringType }
        )) {
            val resultViewModel = getViewModel<ResultViewModel>()
            val pagingFizzBuzz = resultViewModel.fizzBuzzPagingResult.collectAsLazyPagingItems()

            ResultScreen(
                navBack = {
                    navController.popBackStack()
                },
                number1 = it.arguments?.getString(NUMBER1_ARGUMENT_KEY) ?: "",
                number2 = it.arguments?.getString(NUMBER2_ARGUMENT_KEY) ?: "",
                word1 = it.arguments?.getString(WORD1_ARGUMENT_KEY) ?: "",
                word2 = it.arguments?.getString(WORD2_ARGUMENT_KEY) ?: "",
                limit = it.arguments?.getString(LIMIT_ARGUMENT_KEY) ?: "",
                pagingFizzBuzz = pagingFizzBuzz,
                getFizzBuzzPaginated = resultViewModel::getFizzBuzzPaginated
            )
        }
    }
}