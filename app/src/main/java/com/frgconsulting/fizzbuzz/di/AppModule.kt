package com.frgconsulting.fizzbuzz.di

import com.frgconsulting.fizzbuzz.data.FizzBuzzRepositoryImpl
import com.frgconsulting.fizzbuzz.domain.repository.FizzBuzzRepository
import com.frgconsulting.fizzbuzz.domain.usecases.FizzBuzzUseCase
import com.frgconsulting.fizzbuzz.domain.usecases.ValidateNumberUseCase
import com.frgconsulting.fizzbuzz.domain.usecases.ValidateWordUseCase
import com.frgconsulting.fizzbuzz.presentation.home.HomeViewModel
import com.frgconsulting.fizzbuzz.presentation.result.ResultViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

class AppModule {

    private val fizzBuzzRepositoryModule = module {
        single<FizzBuzzRepository> {
            FizzBuzzRepositoryImpl()
        }
    }

    private val validateNumberUseCaseModule = module {
        single {
            ValidateNumberUseCase()
        }
    }
    private val validateWordUseCaseModule = module {
        single {
            ValidateWordUseCase()
        }
    }

    private val fizzBuzzUseCaseModule = module {
        single {
            FizzBuzzUseCase(get())
        }
    }

    private val homeViewModelModule = module {
        viewModel {
            HomeViewModel(
                validateNumber = get(),
                validateWord = get()
            )
        }
    }

    private val resultViewModelModule = module {
        viewModel {
            ResultViewModel(
                fizzBuzz = get()
            )
        }
    }

    val modules = listOf(
        fizzBuzzRepositoryModule,
        validateNumberUseCaseModule,
        validateWordUseCaseModule,
        fizzBuzzUseCaseModule,
        homeViewModelModule,
        resultViewModelModule
    )
}