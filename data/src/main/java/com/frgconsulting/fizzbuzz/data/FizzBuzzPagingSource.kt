package com.frgconsulting.fizzbuzz.data

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState

class FizzBuzzPagingSource(
    private val number1: Int,
    private val number2: Int,
    private val word1: String,
    private val word2: String,
    private val limit: Int,
    private val fizzBuzz: (
        number1: Int,
        number2: Int,
        word1: String,
        word2: String,
        limit: IntRange
    ) -> List<String>
) : PagingSource<Int, String>() {

    companion object {
        const val PAGE_SIZE = 100
    }

    override fun getRefreshKey(state: PagingState<Int, String>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, String> {
        return try {
            val currentPage = params.key ?: 1
            val nextPage = getNextPage(currentPage, limit)
            val range = getRange(nextPage, currentPage)
            val result = fizzBuzz(number1, number2, word1, word2, range)
            return LoadResult.Page(
                data = result,
                prevKey = if (currentPage == 1) null else currentPage - limit,
                nextKey = nextPage
            )
        } catch (e: Exception) {
            Log.e("range error", e.toString())
            LoadResult.Error(e)
        }
    }

    private fun getRange(nextPage: Int?, currentPage: Int): IntRange {
        return if (nextPage != null) {
            currentPage..nextPage
        } else {
            currentPage..limit
        }
    }

    private fun getNextPage(nextKey: Int, limit: Int): Int? {
        return if (nextKey < limit) {
            nextKey + PAGE_SIZE
        } else {
            null
        }
    }

}