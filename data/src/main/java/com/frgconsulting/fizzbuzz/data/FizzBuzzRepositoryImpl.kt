package com.frgconsulting.fizzbuzz.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.frgconsulting.fizzbuzz.domain.repository.FizzBuzzRepository
import kotlinx.coroutines.flow.Flow

class FizzBuzzRepositoryImpl() : FizzBuzzRepository {

    override fun loadFizzBuzzPaginated(
        number1: Int,
        number2: Int,
        word1: String,
        word2: String,
        limit: Int,
        fizzBuzz: (Int, Int, String, String, IntRange) -> List<String>
    ): Flow<PagingData<String>> = Pager(
        config = PagingConfig(
            pageSize = 100
        ),
        pagingSourceFactory = {
            FizzBuzzPagingSource(
                number1 = number1,
                number2 = number2,
                word1 = word1,
                word2 = word2,
                limit = limit,
                fizzBuzz
            )
        }
    ).flow
}
