package com.frgconsulting.fizzbuzz.domain.models

class ValidationResult(
    val isValid: Boolean,
    val errorMessage: String? = null
)