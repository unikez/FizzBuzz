package com.frgconsulting.fizzbuzz.domain.repository

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow

interface FizzBuzzRepository {

    fun loadFizzBuzzPaginated(
        number1: Int,
        number2: Int,
        word1: String,
        word2: String,
        limit: Int,
        fizzBuzz: (Int, Int, String, String, IntRange) -> List<String>
    ): Flow<PagingData<String>>
}