package com.frgconsulting.fizzbuzz.domain.usecases

import androidx.paging.PagingData
import com.frgconsulting.fizzbuzz.domain.repository.FizzBuzzRepository
import kotlinx.coroutines.flow.Flow

class FizzBuzzUseCase(private val fizzBuzzRepository: FizzBuzzRepository) {

    fun execute(
        number1: Int,
        number2: Int,
        word1: String,
        word2: String,
        limit: Int
    ): Flow<PagingData<String>> = fizzBuzzRepository.loadFizzBuzzPaginated(
        number1,
        number2,
        word1,
        word2,
        limit,
        fizzBuzz = { n1, n2, w1, w2, r ->
            fizzBuzz(n1, n2, w1, w2, r)
        })

    fun fizzBuzz(
        number1: Int,
        number2: Int,
        word1: String,
        word2: String,
        limit: IntRange
    ): List<String> {
        val result = mutableListOf<String>()
        for (i in limit) {

            val isMultipleOfNumber1 = i % number1 == 0
            val isMultipleOfNumber2 = i % number2 == 0

            when {
                isMultipleOfNumber1 && isMultipleOfNumber2 -> {
                    result.add("$word1$word2")
                }

                isMultipleOfNumber1 -> {
                    result.add(word1)
                }

                isMultipleOfNumber2 -> {
                    result.add(word2)
                }

                else -> {
                    result.add(i.toString())
                }
            }
        }
        return result
    }
}