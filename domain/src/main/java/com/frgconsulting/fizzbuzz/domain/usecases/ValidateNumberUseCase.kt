package com.frgconsulting.fizzbuzz.domain.usecases

import com.frgconsulting.fizzbuzz.domain.models.ValidationResult

class ValidateNumberUseCase {

    companion object {
        const val ERROR_MESSAGE_INVALID = "Entrer un nombre"
        const val ERROR_MESSAGE_NEGATIVE = "Entrer un nombre supérieur à 0"
        const val ERROR_MESSAGE_VALID = "Entrer un nombre valide"
    }

    fun execute(number: String): ValidationResult {
        if (number.isBlank()) {
            return ValidationResult(false, ERROR_MESSAGE_INVALID)
        }

        if (number.toIntOrNull() == null) {
            return ValidationResult(false, ERROR_MESSAGE_VALID)
        }

        if (number.toInt() < 1) {
            return ValidationResult(false, ERROR_MESSAGE_NEGATIVE)
        }

        return ValidationResult(true)
    }
}