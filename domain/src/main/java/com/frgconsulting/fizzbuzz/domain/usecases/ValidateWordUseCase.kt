package com.frgconsulting.fizzbuzz.domain.usecases

import com.frgconsulting.fizzbuzz.domain.models.ValidationResult

class ValidateWordUseCase {

    companion object {
        const val ERROR_BLANK_MESSAGE = "Entrer un mot"
        const val ERROR_LENGHT = "Entrer un mot d'au moins 2 caractères"
    }

    fun execute(word: String): ValidationResult {
        if(word.isBlank()) {
            return ValidationResult(false, ERROR_BLANK_MESSAGE)
        }

        if(word.length < 2) {
            return ValidationResult(false, ERROR_LENGHT)
        }

        return ValidationResult(true)
    }
}