package com.frgconsulting.fizzbuzz.domain.usecases

import com.frgconsulting.fizzbuzz.domain.repository.FizzBuzzRepository
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock

class FizzBuzzTest {

    private val fizzBuzzRepository: FizzBuzzRepository = mock()
    private val fizzBuzzUseCase = FizzBuzzUseCase(fizzBuzzRepository)

    @Test
    fun `execute should return a correct result when given a valid input`() {
        val number1 = 3
        val number2 = 5
        val word1 = "Fizz"
        val word2 = "Buzz"
        val limit = 20

        val expectedResult = listOf(
            "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz",
            "Buzz", "11", "Fizz", "13", "14", "FizzBuzz", "16", "17",
            "Fizz", "19", "Buzz"
        )

        val result = fizzBuzzUseCase.fizzBuzz(number1, number2, word1, word2, 1..limit)

        assertEquals(
            expectedResult,
            result
        )
    }
}