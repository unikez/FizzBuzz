package com.frgconsulting.fizzbuzz.domain.usecases

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ValidateNumberTest {

    private lateinit var validateNumber: ValidateNumberUseCase

    @Before
    fun setUp() {
        validateNumber = ValidateNumberUseCase()
    }

    @Test
    fun `execute should return false and error message when number is blank`() {
        val number = ""

        val result = validateNumber.execute(number)

        assertFalse(result.isValid)
        assertEquals("Entrer un nombre", result.errorMessage)
    }

    @Test
    fun `execute should return false and error message when number is not a number`() {
        val number = "a"

        val result = validateNumber.execute(number)

        assertFalse(result.isValid)
        assertEquals("Entrer un nombre valide", result.errorMessage)
    }

    @Test
    fun `execute should return false and error message when number is less than 1`() {
        val number = "0"

        val result = validateNumber.execute(number)

        assertFalse(result.isValid)
        assertEquals("Entrer un nombre supérieur à 0", result.errorMessage)
    }
}