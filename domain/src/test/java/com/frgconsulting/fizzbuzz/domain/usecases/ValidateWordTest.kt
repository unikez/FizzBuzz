package com.frgconsulting.fizzbuzz.domain.usecases

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ValidateWordTest {

    private lateinit var validateWord: ValidateWordUseCase


    @Before
    fun setUp() {
        validateWord = ValidateWordUseCase()
    }

    @Test
    fun `execute should return false and error message when word is blank`() {
        val word = ""

        val result = validateWord.execute(word)

        assertFalse(result.isValid)
        assertEquals("Entrer un mot", result.errorMessage)
    }

    @Test
    fun `execute should return false and error message when word is less than 2 characters`() {
        val word = "a"

        val result = validateWord.execute(word)

        assertFalse(result.isValid)
        assertEquals("Entrer un mot d'au moins 2 caractères", result.errorMessage)
    }
}