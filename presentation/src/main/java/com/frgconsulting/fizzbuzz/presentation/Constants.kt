package com.frgconsulting.fizzbuzz.presentation

object Constants {
    const val NUMBER1_ARGUMENT_KEY = "number1"
    const val NUMBER2_ARGUMENT_KEY = "number2"
    const val WORD1_ARGUMENT_KEY = "word1"
    const val WORD2_ARGUMENT_KEY = "word2"
    const val LIMIT_ARGUMENT_KEY = "limit"
}