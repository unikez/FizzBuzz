package com.frgconsulting.fizzbuzz.presentation

import com.frgconsulting.fizzbuzz.presentation.Constants.LIMIT_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Constants.NUMBER1_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Constants.NUMBER2_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Constants.WORD1_ARGUMENT_KEY
import com.frgconsulting.fizzbuzz.presentation.Constants.WORD2_ARGUMENT_KEY

sealed class Screens(val route: String) {
    object Home : Screens("home_screen")
    object Result :
        Screens("result_screen/{$NUMBER1_ARGUMENT_KEY}/{$NUMBER2_ARGUMENT_KEY}/{$WORD1_ARGUMENT_KEY}/{$WORD2_ARGUMENT_KEY}/{$LIMIT_ARGUMENT_KEY}") {
        fun createRoute(
            number1: String,
            number2: String,
            word1: String,
            word2: String,
            limit: String
        ): String {
            return "result_screen/$number1/$number2/$word1/$word2/$limit"
        }
    }
}