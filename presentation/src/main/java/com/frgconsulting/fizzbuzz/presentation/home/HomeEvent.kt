package com.frgconsulting.fizzbuzz.presentation.home

sealed class HomeEvent {
    data class OnNumber1Change(val number1: String) : HomeEvent()
    data class OnNumber2Change(val number2: String) : HomeEvent()
    data class OnWord1Change(val word1: String) : HomeEvent()
    data class OnWord2Change(val word2: String) : HomeEvent()
    data class OnLimitChange(val limit: String) : HomeEvent()
    data class OnSubmit(val onSuccess: (String) -> Unit) : HomeEvent()
}