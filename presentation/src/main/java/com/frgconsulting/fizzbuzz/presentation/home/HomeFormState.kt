package com.frgconsulting.fizzbuzz.presentation.home

data class HomeFormState(
    val number1: String = "",
    val number1Error: String? = null,
    val number2: String = "",
    val number2Error: String? = null,
    val word1: String = "",
    val word1Error: String? = null,
    val word2: String = "",
    val word2Error: String? = null,
    val limit: String = "",
    val limitError: String? = null,
    val result: List<String> = emptyList()
)