package com.frgconsulting.fizzbuzz.presentation.home

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.frgconsulting.fizzbuzz.presentation.components.CustomAppBar
import com.frgconsulting.fizzbuzz.presentation.components.CustomTextField

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(
    state: HomeFormState,
    onEvent: (HomeEvent) -> Unit,
    navigateTo: (String) -> Unit
) {

    Scaffold(
        topBar = {
            CustomAppBar(title = "FIZZBUZZ")
        }
    ) { paddingValues ->
        Surface(
            Modifier
                .padding(paddingValues)
                .fillMaxSize()
        ) {
            Column(
                modifier = Modifier
                    .padding(24.dp)
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                CustomTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = "Nombre 1",
                    placeholder = "Entrez un nombre",
                    value = state.number1,
                    error = state.number1Error,
                    keyboardType = KeyboardType.Number,
                    onValueChange = {
                        onEvent(HomeEvent.OnNumber1Change(it))
                    }
                )
                Spacer(modifier = Modifier.height(16.dp))
                CustomTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = "Nombre 2",
                    placeholder = "Entrez un nombre",
                    value = state.number2,
                    error = state.number2Error,
                    keyboardType = KeyboardType.Number,
                    onValueChange = {
                        onEvent(HomeEvent.OnNumber2Change(it))
                    }
                )
                Spacer(modifier = Modifier.height(16.dp))
                CustomTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = "Mot 1",
                    placeholder = "Entrez un mot",
                    value = state.word1,
                    error = state.word1Error,
                    onValueChange = {
                        onEvent(HomeEvent.OnWord1Change(it))
                    }
                )
                Spacer(modifier = Modifier.height(16.dp))
                CustomTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = "Mot 2",
                    value = state.word2,
                    placeholder = "Entrez un mot",
                    error = state.word2Error,
                    onValueChange = {
                        onEvent(HomeEvent.OnWord2Change(it))
                    }
                )
                Spacer(modifier = Modifier.height(16.dp))
                CustomTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = "Limite",
                    placeholder = "Entrez un nombre",
                    value = state.limit,
                    error = state.limitError,
                    keyboardType = KeyboardType.Number,
                    onValueChange = {
                        onEvent(HomeEvent.OnLimitChange(it))
                    }
                )
                Spacer(modifier = Modifier.height(24.dp))
                Button(modifier = Modifier.fillMaxWidth(), onClick = {
                    onEvent(HomeEvent.OnSubmit {
                        navigateTo(it)
                    })
                }) {
                    Text(text = "Valider")
                }
            }
        }
    }
}