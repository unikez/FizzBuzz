package com.frgconsulting.fizzbuzz.presentation.home

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.frgconsulting.fizzbuzz.domain.usecases.FizzBuzzUseCase
import com.frgconsulting.fizzbuzz.domain.usecases.ValidateNumberUseCase
import com.frgconsulting.fizzbuzz.domain.usecases.ValidateWordUseCase
import com.frgconsulting.fizzbuzz.presentation.Screens

class HomeViewModel(
    private val validateNumber: ValidateNumberUseCase,
    private val validateWord: ValidateWordUseCase,
) : ViewModel() {

    var state by mutableStateOf(HomeFormState())

    fun onEvent(event: HomeEvent) {
        when (event) {
            is HomeEvent.OnNumber1Change -> {
                state = state.copy(number1 = event.number1)
            }

            is HomeEvent.OnNumber2Change -> {
                state = state.copy(number2 = event.number2)
            }

            is HomeEvent.OnSubmit -> {
                submit(event.onSuccess)
            }

            is HomeEvent.OnWord1Change -> {
                state = state.copy(word1 = event.word1)
            }

            is HomeEvent.OnWord2Change -> {
                state = state.copy(word2 = event.word2)
            }

            is HomeEvent.OnLimitChange -> {
                state = state.copy(limit = event.limit)
            }
        }
    }

    private fun submit(onSuccess: (String) -> Unit = {}) {
        val number1Result = validateNumber.execute(state.number1)
        val number2Result = validateNumber.execute(state.number2)
        val limitResult = validateNumber.execute(state.limit)
        val word1Result = validateWord.execute(state.word1)
        val word2Result = validateWord.execute(state.word2)

        state = state.copy(
            number1Error = number1Result.errorMessage,
            number2Error = number2Result.errorMessage,
            word1Error = word1Result.errorMessage,
            word2Error = word2Result.errorMessage,
            limitError = limitResult.errorMessage
        )

        val isFormValid = listOf(
            number1Result.isValid,
            number2Result.isValid,
            limitResult.isValid,
            word1Result.isValid,
            word2Result.isValid
        ).all { it }

        if (!isFormValid) {
            return
        }

        onSuccess(
            Screens.Result.createRoute(
                number1 = state.number1,
                number2 = state.number2,
                word1 = state.word1,
                word2 = state.word2,
                limit = state.limit
            )
        )
    }
}