package com.frgconsulting.fizzbuzz.presentation.result

import android.graphics.drawable.Icon
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.itemContentType
import androidx.paging.compose.itemKey
import com.frgconsulting.fizzbuzz.presentation.components.CustomAppBar
import com.frgconsulting.fizzbuzz.presentation.components.ErrorContent
import com.frgconsulting.fizzbuzz.presentation.components.LoadingContent

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ResultScreen(
    navBack: () -> Unit,
    number1: String,
    number2: String,
    word1: String,
    word2: String,
    limit: String,
    getFizzBuzzPaginated: (String, String, String, String, String) -> Unit,
    pagingFizzBuzz: LazyPagingItems<String>
) {

    LaunchedEffect(key1 = true) {
        getFizzBuzzPaginated(number1, number2, word1, word2, limit)
    }

    Scaffold(
        topBar = {
            CustomAppBar(title = "RESULT", backIcon = true, navBack = navBack)
        }
    ) { paddingValues ->
        Surface(
            Modifier
                .padding(paddingValues)
                .fillMaxSize()
        ) {

            LazyColumn() {
                items(
                    count = pagingFizzBuzz.itemCount,
                    key = pagingFizzBuzz.itemKey(),
                    contentType = pagingFizzBuzz.itemContentType(
                    ),
                ) { index ->
                    val item = pagingFizzBuzz[index]
                    if (item != null) {
                        Text(
                            text = item,
                            modifier = Modifier
                                .padding(16.dp)
                                .fillMaxWidth(),
                            style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold)
                        )
                    }
                }
            }

            pagingFizzBuzz.apply {
                when {
                    loadState.refresh is LoadState.Loading -> {
                        LoadingContent()
                    }
                    loadState.refresh is LoadState.Error -> {
                        ErrorContent(message = (loadState.refresh as LoadState.Error).error.message)
                    }
                    loadState.append is LoadState.Loading -> {
                        LoadingContent()
                    }
                    loadState.append is LoadState.Error -> {
                        ErrorContent(message = (loadState.append as LoadState.Error).error.message)
                    }
                }
            }
        }
    }
}