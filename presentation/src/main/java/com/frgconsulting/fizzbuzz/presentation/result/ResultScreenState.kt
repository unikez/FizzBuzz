package com.frgconsulting.fizzbuzz.presentation.result

data class ResultScreenState(
    val result: List<String> = emptyList(),
)