package com.frgconsulting.fizzbuzz.presentation.result

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.frgconsulting.fizzbuzz.domain.usecases.FizzBuzzUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOn

class ResultViewModel(
    private val fizzBuzz: FizzBuzzUseCase
) : ViewModel() {

    var state by mutableStateOf(ResultScreenState())
    var fizzBuzzPagingResult by mutableStateOf<Flow<PagingData<String>>>(emptyFlow())

    fun getFizzBuzzPaginated(
        number1: String,
        number2: String,
        word1: String,
        word2: String,
        limit: String
    ) {
        fizzBuzzPagingResult = fizzBuzz.execute(
            number1 = number1.toInt(),
            number2 = number2.toInt(),
            word1 = word1,
            word2 = word2,
            limit = limit.toInt()
        ).cachedIn(viewModelScope)
    }
}